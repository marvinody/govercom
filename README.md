## Silly thing
Friend wanted to vertically combine images from a Korean webcomic so I figured to write it in Go
So we end up with the GoVerCom.

## Usage
Compile and drop the binary in the top level of a directory with a bunch of manga chapters
Run it (with an optional max height as the only arg)
Will search all the top level dirs (it considers those as chapters/volumes) and recurse down finding all images and stitch them together
It will then create an out dir with corresponding folders and create the image files with zeros prepended.
