package main

import (
	"image"
	"image/color"
	"image/png"
	"os"
	"path/filepath"
	"sort"
	"testing"
)

func TestGetTopLevelDirs(t *testing.T) {
	td := filepath.Join(".", "testdata")
	fiCI, err := os.Stat(filepath.Join(td, "CombineImages"))
	if err != nil {
		t.Fatal(err)
	}
	fiFIND, err := os.Stat(filepath.Join(td, "FindImagesInDir"))
	if err != nil {
		t.Fatal(err)
	}
	fiGLW, err := os.Stat(filepath.Join(td, "GetLargestWidth"))
	if err != nil {
		t.Fatal(err)
	}
	dirs, err := getTopLevelDirs(td)
	expectedDirs := []os.FileInfo{
		fiCI, fiFIND, fiGLW,
	}
	if len(dirs) != len(expectedDirs) {
		t.Fatalf("Expected %d dirs, but found %d", len(expectedDirs), len(dirs))
	}
	for idx := range dirs {
		d, ed := dirs[idx].fi, expectedDirs[idx]
		if d.Name() != ed.Name() {
			t.Errorf("Expected %s but got %s", ed.Name(), d.Name())
		}
	}
}

/*
func TestCombineImages(t *testing.T) {
	ci := filepath.Join(".", "testdata", "CombineImages")

	testData := []string{
		filepath.Join(ci, "teal.jpg"),
		filepath.Join(ci, "red.jpg"),
		filepath.Join(ci, "blue.png"),
		filepath.Join(ci, "yellow.jpg"),
	}
	combined, err := combineImages(testData, 20, 40)
	if err != nil {
		t.Fatal(err)
	}
	if len(combined) != 2 {
		t.Fatalf("Expected 2 combined images, but got %d", len(combined))
	}
	testImages(t, filepath.Join(ci, "1_teal_red.png"), combined[0])
	testImages(t, filepath.Join(ci, "2_blue_yellow.png"), combined[1])
}
*/
func TestFindImagesInDir(t *testing.T) {
	testdir := filepath.Join(".", "testdata", "FindImagesInDir")
	fi, err := os.Stat(testdir)
	if err != nil {
		t.Fatal(err)
	}
	cwd, _ := os.Getwd()
	images, err := findImagesInDir(filepath.Join(cwd, "testdata"), fi)
	if err != nil {
		t.Fatal(err)
	}
	sort.Slice(images, func(i int, j int) bool {
		return images[i] < images[j]
	})
	expectedCount := 8
	if len(images) != expectedCount {
		t.Fatalf("Expected %d images, got %d", expectedCount, len(images))
	}
}

/*
func TestGetLargestWidth(t *testing.T) {
	glw := filepath.Join(".", "testdata", "GetLargestWidth")
	testData := []string{
		filepath.Join(glw, "2.png"),
		filepath.Join(glw, "4.jpg"),
		filepath.Join(glw, "2000.jpg"),
	}
	expectedWidth := 2000
	actualWidth, err := getLargestWidth(testData)
	if err != nil {
		t.Fatal(err)
	}
	if expectedWidth != actualWidth {
		t.Fatalf("Expected %d, but got %d", expectedWidth, actualWidth)
	}
}
*/
func testImages(t *testing.T, loc string, img image.Image) {
	file, err := os.Open(loc)
	if err != nil {
		t.Error(err)
	}
	expectedImg, err := png.Decode(file)
	if err != nil {
		t.Error(err)
	}
	actualImg := img
	actualBounds, expectedBounds := actualImg.Bounds(), expectedImg.Bounds()
	if actualBounds.Dx() != expectedBounds.Dx() || actualBounds.Dy() != expectedBounds.Dy() {
		t.Fatalf("Expected dims: [%d,%d]. Actual: [%d, %d]",
			expectedBounds.Dx(),
			expectedBounds.Dy(),
			actualBounds.Dx(),
			actualBounds.Dy(),
		)
	}
	for y := 0; y < actualBounds.Dy(); y += 1 {
		for x := 0; x < actualBounds.Dx(); x += 1 {
			actualColor := actualImg.At(x, y)
			expectedColor := expectedImg.At(x, y)
			if !colorEquals(actualColor, expectedColor) {
				t.Errorf("Color mismatch at [%d,%d]. Expected %v, but got %v", x, y, expectedColor, actualColor)
			}
		}
	}
}

func colorEquals(A, B color.Color) bool {
	AR, AG, AB, AA := A.RGBA()
	BR, BG, BB, BA := B.RGBA()
	return AR == BR && AG == BG && AB == BB && AA == BA
}
