package main

import (
	"fmt"
	"image"
	"image/draw"
	"image/jpeg"
	_ "image/png"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
)

func main() {
	maxHeight := 60000
	if len(os.Args) > 1 {
		newHeight, err := strconv.Atoi(os.Args[1])
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s was given but expected an integer or nothing\n", os.Args[1])
			return
		}
		maxHeight = newHeight
	} else {
		fmt.Fprintf(os.Stdout, "No max height was given, using %d pixels as max\n", maxHeight)
	}

	outDir := filepath.Join(".", "out")
	os.Mkdir(outDir, 0700)

	mangaDirs, err := getTopLevelDirs(".")
	if err != nil {
		panic(err)
	}

	for _, mangaCfi := range mangaDirs {
		dirpath := filepath.Join(mangaCfi.path, mangaCfi.fi.Name())
		fmt.Fprintf(os.Stdout, "Searching for images in \"%s\"\n", dirpath)
		imageList, err := findImagesInDir(mangaCfi.path, mangaCfi.fi)
		if err != nil {
			panic(err)
		}
		fmt.Fprintf(os.Stdout, "%d images found, chunking images into batches and finding width\n", len(imageList))

		chunked, width, err := chunkImages(imageList, maxHeight)
		fmt.Fprintf(os.Stdout, "Max width: %d, chunks: %d \n", width, len(chunked))

		fmt.Fprintf(os.Stdout, "stitching batches together And writing\n")
		outMangaDir := filepath.Join(outDir, mangaCfi.fi.Name())
		os.Mkdir(outMangaDir, 0700)
		err = combineImagesAndWrite(chunked, width, outMangaDir)
		if err != nil {
			panic(err)
		}
	}
}

// getTopLevelDirs will pull all the things are dirs and not named "out"
func getTopLevelDirs(path string) ([]custFileInfo, error) {
	dirs, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}
	cfiArr := make([]custFileInfo, 0, len(dirs))
	for _, dir := range dirs {
		if !dir.IsDir() || dir.Name() == "out" {
			continue
		}
		cfiArr = append(cfiArr, custFileInfo{dir, path})
	}
	sort.Slice(cfiArr, func(i int, j int) bool {
		return cfiArr[i].fi.Name() < cfiArr[j].fi.Name()
	})
	return cfiArr, nil
}

func writeImage(img image.Image, idx int, outMangaDir string) error {
	name := fmt.Sprintf("%03d.jpg", idx+1)
	filename := filepath.Join(outMangaDir, name)
	hdl, err := os.Create(filename)
	defer hdl.Close()
	if err != nil {
		return err
	}
	jpeg.Encode(hdl, img, &jpeg.Options{90})
	return nil

}

func chunkImages(imageList []string, maxHeight int) ([]imageChunk, int, error) {
	chunks := make([]imageChunk, 0, 8)
	curChunk := imageChunk{}
	curHeight := 0
	largestWidth := 0
	for _, filename := range imageList {
		conf, err := readImageConfig(filename)
		if err != nil {
			return nil, 0, err
		}
		if curHeight+conf.Height > maxHeight {
			// break list and make new
			curChunk.height = curHeight
			chunks = append(chunks, curChunk)
			curChunk = imageChunk{}
			curHeight = 0
		}

		if conf.Width > largestWidth {
			largestWidth = conf.Width
		}

		curChunk.imageList = append(curChunk.imageList, filename)
		curHeight += conf.Height
	}
	// handle last one not being full
	if curHeight > 0 {
		curChunk.height = curHeight
		chunks = append(chunks, curChunk)
	}
	return chunks, largestWidth, nil
}

func readImageConfig(fullpath string) (image.Config, error) {
	// read and decode config
	reader, err := os.Open(fullpath)
	defer reader.Close()
	if err != nil {
		return image.Config{}, err
	}
	conf, _, err := image.DecodeConfig(reader)
	if err != nil {
		return image.Config{}, err
	}
	return conf, nil
}

func readImage(fullpath string) (image.Image, error) {
	// read and decode
	reader, err := os.Open(fullpath)
	defer reader.Close()
	if err != nil {
		return nil, err
	}
	img, _, err := image.Decode(reader)
	if err != nil {
		return nil, err
	}
	return img, nil
}

type imageChunk struct {
	imageList []string
	height    int
}

func combineImagesAndWrite(imageChunkList []imageChunk, width int, outMangaDir string) error {
	for i, chunk := range imageChunkList {
		curCollage := image.NewRGBA(image.Rect(0, 0, width, chunk.height))
		curHeight := 0
		for _, filename := range chunk.imageList {

			img, err := readImage(filename)
			if err != nil {
				return err
			}
			// get pos for final collage
			leftShift := (width - img.Bounds().Dx()) / 2
			topShift := curHeight
			rectToDraw := img.Bounds().Add(image.Point{leftShift, topShift})
			// actually draw
			draw.Draw(
				curCollage, // drawing onto what
				rectToDraw, // in which region
				img,        // putting what
				image.ZP,   // starting where?
				draw.Src,   // some op thing
			)
			// inc for next iter
			curHeight += img.Bounds().Dy()
		}
		writeImage(curCollage, i, outMangaDir)
	}

	return nil
}

func findImagesInDir(path string, info os.FileInfo) ([]string, error) {
	imageRegex := regexp.MustCompile(`\.(jpg|jpeg|png)$`) // pretty unsafe but don't expect bugs
	images := make([]string, 0, 16)

	root := filepath.Join(path, info.Name())
	filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if imageRegex.MatchString(info.Name()) {
			images = append(images, path)
		}
		return nil
	})
	return images, nil
}

type custFileInfo struct {
	fi   os.FileInfo
	path string
}
